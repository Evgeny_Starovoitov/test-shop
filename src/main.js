// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueResource from 'vue-resource';

Vue.use(VueResource);
Vue.config.productionTip = false

import { createStore } from './store'

const store = createStore();

new Vue({
  el: '#app',
  store,
  components: { App },
  template: '<App/>'
})
