import Vue from 'vue'
import Vuex from 'vuex'
import CryptoJS from 'crypto-js'
Vue.use(Vuex)

export function createStore(tData) {
	return new Vuex.Store({
		state: {
			goods: [],
			goodsHash: '',
			cart: {}
		},
		mutations: {
			// добавление / удаление элементов в корзине
			'ADD_TO_CART'(state, item) {
				if (state.cart[item.url]) {
					Vue.delete(state.cart, item.url);
				} else {
					Vue.set(state.cart, item.url, item);
				}
			},
			// очистка корзины
			'CLEAR_CART'(state) {
				Vue.set(state, 'cart', []);
			}
		},
		actions: {
			// добавление / удаление элементов в корзине
			addToCart(context, item) {
				return new Promise((resolve, reject) => {
					if (item && item.url) {
						context.commit('ADD_TO_CART', item);
					}
					resolve();
				});
			},
			// обновление товаров
			updateGoods(context) {
				return new Promise((resolve, reject) => {
					Vue.http.get('https://swapi.co/api/starships?callback=lol')
					.then((resp)=>{
						let _goods = resp.body.results;
						let hash = CryptoJS.SHA256(JSON.stringify(_goods)).toString();
						// проставляем случайные цены
						for (let i = 0; i < _goods.length;i++) {
							_goods[i].price = (Math.random() * 1000).toFixed(2);
						}
						// если что то в товарах обновилось - заменяем на новое
						if (hash !== context.state.goodsHash) {
							Vue.set(context.state, 'goods', _goods);
							Vue.set(context.state, 'goodsHash', hash);
						}
						Vue.nextTick(()=>{
							resolve();
						});
					})
					.catch((e)=>{
						reject(e);
					})
				});
			}
		}
	})
}