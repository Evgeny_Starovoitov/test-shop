export default {
	data() {
		return {
			// переменная нужная в основном дл япредотвращения "нескольких нажатий" актуально при постах к серверу
			apiBusy: false
		}
	},
	computed: {
		cart() {
			return this.$store.state.cart;
		},
		total() {
			let out = {
				count: 0,
				summ: 0
			}
			for (let key in this.cart) {
				let price = (this.cart[key] && this.cart[key].price) ? this.cart[key].price : 0;
				out.count++;
				out.summ+= Number(price);
			}
			return out;
		}
	},
	methods: {
		addToCart(item) {
			if (this.apiBusy) return;
			this.apiBusy = true;
			this.$store.dispatch('addToCart', item)
			.then(()=>{
				this.apiBusy = false;
			})
		}
	}
}